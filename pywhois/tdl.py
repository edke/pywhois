import datetime
import re


class Base(object):
    regulars = {}
    date_format = '%Y-%m-%d'
    statuses = {}

    def __init__(self):
        self.raw = ''

    def parse(self, pattern):
        if pattern not in self.regulars:
            return None

        match = re.search(self.regulars[pattern], self.raw)
        if match:
            return match.group(1)
        else:
            return None

    @property
    def name(self):
        return self.parse('name')

    @property
    def registrar_handle(self):
        return self.parse('registrar_handle')

    @property
    def registrar_email(self):
        return self.parse('registrar_email')

    @property
    def technical_handle(self):
        return self.parse('technical_handle')

    @property
    def technical_email(self):
        return self.parse('technical_email')

    @property
    def dns_name1(self):
        return self.parse('dns_name1')

    @property
    def dns_name2(self):
        return self.parse('dns_name2')

    @property
    def dns_name3(self):
        return self.parse('dns_name3')

    @property
    def expire_date(self):
        str_date = self.parse('expire_date')
        if str_date is not None:
            return datetime.datetime.strptime(str_date, self.date_format)
        else:
            return None

    @property
    def last_update(self):
        str_date = self.parse('last_update')
        if str_date is not None:
            return datetime.datetime.strptime(str_date, self.date_format)
        else:
            return None

    @property
    def status(self):
        orig_status = self.parse('status')
        if orig_status in self.statuses:
            return self.statuses[orig_status]
        else:
            return None

    def set_raw(self, raw):
        self.raw = raw


class Sk(Base):
    tld = '.sk'

    regulars = {
        'name': r'Domain-name\s*(.+)',
        'registrar_handle': r'Admin-id\s*(.+)',
        'registrar_email': r'Admin-email\s*(.+)',
        'technical_handle': r'Tech-id\s*(.+)',
        'technical_email': r'Tech-email\s*(.+)',
        'dns_name1': r'dns_name\s*(.+)',
        'dns_name2': r'dns_name\s*.+\s*dns_name\s*(.+)',
        'dns_name3': r'dns_name\s*.+\s*dns_name\s*.+\s*dns_name\s*(.+)',
        'expire_date': r'Valid-date\s*(.+)',
        'last_update': r'Last-update\s*(.+)',
        'status': r'Domain-status\s*(.+)',
    }

    date_format = '%Y-%m-%d'

    statuses = {
        'DOM_LOCK': 'EXPIRED',
        'DOM_TA': 'OK',
        'DOM_DAKT': 'EXPIRED',
        'DOM_OK': 'OK',
        'DOM_WARN': 'WARNED',
        'DOM_LNOT': 'WARNED',
        'DOM_EXP': 'EXPIRED',
        'DOM_HELD': 'EXPIRED',
        'DOM_DEL': 'EXPIRED',
        'DOM_TRAN': 'OK',
        'DOM_TRDACT': 'EXPIRED',
        'DOM_RLOST': 'EXPIRED',
    }


class Uk(Base):
    tld = '.co.uk'

    regulars = {
        'name': r'Domain name:\s*(.+)',
        'registrar_handle': r'Registrant:\s*(.+)',
    }

