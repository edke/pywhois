import re
import subprocess
from pywhois.tdl import Sk, Uk


class Whois(object):

    def __init__(self):
        self.tld = {}

        self.register(Sk())
        self.register(Uk())

    def query(self, domain):
        pattern = '(%s)$' % '|'.join(['%s' % x.replace('.', '\.') for x in self.tld])
        match = re.search(pattern, domain)
        if not match:
            raise Exception('Domain %s is not supported' % domain)

        proc = subprocess.Popen(['whois', domain], stdout=subprocess.PIPE)
        return_code = proc.wait()
        out, err = proc.communicate()

        if return_code == 0 and not re.search(r'Not found.', out):
            tld = self.tld[match.group(0)]
            tld.set_raw(out)
            return tld
        else:
            return False

    def register(self, tld):
        if tld.tld in self.tld:
            raise Exception('TDL .%s already registered' % tld.tld)
        self.tld[tld.tld] = tld
