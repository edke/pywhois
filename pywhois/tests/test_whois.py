import unittest
import datetime
from pywhois import Whois
from pywhois.tdl import Sk


class TestWhois(unittest.TestCase):

    def setUp(self):
        self.whois = Whois()

    def test_sk(self):
        query = self.whois.query('sknic.sk')

        self.assertIsInstance(query, Sk)
        self.assertEqual(query.name, 'sknic.sk')
        self.assertEqual(query.registrar_handle, 'VMMI-0001')
        self.assertEqual(query.registrar_email, 'info@vmm.sk')
        self.assertEqual(query.technical_handle, 'I4UI-0001')
        self.assertEqual(query.technical_email, 'dns@i4u.sk')
        self.assertEqual(query.dns_name1, 'ns1.i4u.sk')
        self.assertEqual(query.dns_name2, 'ns2.i4u.sk')
        self.assertEqual(query.dns_name3, None)
        self.assertEqual(query.expire_date, datetime.datetime(2014, 9, 25, 0, 0))
        self.assertEqual(query.last_update, datetime.datetime(2013, 9, 4, 0, 0))
        self.assertEqual(query.status, 'OK')

    def test_uk(self):
        query = self.whois.query('yahoo.co.uk')
        self.assertEqual(query.name, 'yahoo.co.uk')
        self.assertEqual(query.registrar_handle, 'Yahoo! Inc')

    def test_cz(self):
        with self.assertRaises(Exception):
            self.whois.query('nic.cz')


if __name__ == '__main__':
    unittest.main()

    #status = models.CharField(max_length=20, choices=DOMAIN_STATUS)
    #host = models.CharField(max_length=200, blank=True)
    #www = models.CharField(max_length=200, blank=True)
    #mx = models.CharField(max_length=200, blank=True)
